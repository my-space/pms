package com.busi.pm.config;


import com.busi.pm.controller.BoardController;
import com.busi.pm.controller.EquipmentController;
import com.busi.pm.controller.GanttController;
import com.busi.pm.controller.MaterialController;
import com.busi.pm.controller.PersonController;
import com.busi.pm.controller.StationController;
import com.jfinal.config.Routes;

public class PmRoutes extends Routes{

	@Override
	public void config() {		
		add("/board",BoardController.class,"busi/pm/board");
		add("/gantt",GanttController.class,"busi/pm/gantt");
		add("/station", StationController.class, "busi/pm/station");
		add("/person", PersonController.class, "busi/pm/person");
		add("/material", MaterialController.class, "busi/pm/material");
		add("/equipment", EquipmentController.class, "busi/pm/equipment");
	}

}
