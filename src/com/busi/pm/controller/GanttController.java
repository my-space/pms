package com.busi.pm.controller;

import java.util.List;

import com.busi.pm.model.PmEquipment;
import com.busi.pm.model.PmQuestion;
import com.busi.pm.model.PmMaterial;
import com.busi.pm.model.PmPerson;
import com.busi.pm.model.PmSchedule;
import com.busi.pm.model.PmStation;
import com.busi.pm.model.PmWorkLog;
import com.frame.common.util.StringUtil;
import com.frame.controller.BaseController;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class GanttController extends BaseController {
	
	/********************************以下为甘特图**********************************/
	/**
	 * 公共看板首页
	 */
	public void index(){
		setAttr("title", "环线项目管理系统");	
		/*setAttr("sysuser", getSessionUser());
		List<PmStation> ps=PmStation.dao.getStationList();	
		List<Record> data = Db.find("select * from pm_progress_v  order by id asc");	
		setAttr("stations", data);*/
		render("index.html");
	}	
	/**
	 * 所有站点宏观横道图
	 */
	public void getStations(){		
		List<Record> data = Db.find("select * from pm_progress_v");		
		renderDhtmlGantt(data, null);		
	}		
	/**
	 * 详细站点横道图页面
	 * dhtmlgantt技术
	 */
	public void stationPage() {
		String stationId = getPara("station_id");
		if (StringUtil.isNotEmpty(stationId)) {
			setAttr("station_name", PmStation.dao.getStationNameById(stationId));
			setAttr("stationId", stationId);
		}
		setAttr("title", "站点进度查看");
		render("station.html");
	}
	/**
	 * 各站点横道图进度详细数据
	 * dhtmlgantt技术
	 */
	public void getOneSchedule() {
		String stationId = getPara("station_id");
		//从表中获取进度信息
		/*List<Record> data = PmScheduleZh.dao.getScheduleZh(stationId);*/
		//List<Record> links = PmScheduleLinks.dao.getPmScheduleLinks(stationId);
		// List<Record> links=null;
		//从视图中获取进度
		List<Record> data = PmSchedule.dao.getScheduleFromView(stationId);
		renderDhtmlGantt(data, null);
	}
	/**
	 * 公共
	 * 横道图下方数据
	 */
	public void fooeterList() {
		String task_id = getPara("task_id");
		if(StringUtil.isNotEmpty(task_id)){			
			setAttr("persons", PmPerson.dao.getPmPerson(task_id));
			setAttr("equipments", PmEquipment.dao.getEquipments(task_id));
			setAttr("materials", PmMaterial.dao.getPmMaterial(task_id));
			setAttr("worklogs", PmWorkLog.dao.getWorkLog(getPara("station_id")));
			setAttr("questions", PmQuestion.dao.getQuestions(getPara("station_id")));			
		}		
		render("gantt_footer.html");
	}		
	
}
