package com.busi.pm.controller;

import java.util.List;

import com.busi.pm.model.PmSchedule;
import com.frame.common.tool.DBTool;
import com.frame.common.util.StringUtil;
import com.frame.controller.BaseController;
import com.jfinal.plugin.activerecord.Record;

public class StationController extends BaseController {
	public void index(){
		setAttr("station_id", getPara("station_id"));
		render("index.html");		
	}
	public void treeData(){
		String stationId = getPara("station_id");
		//从表中获取进度信息
		/*List<Record> data = PmScheduleZh.dao.getScheduleZh(stationId);*/
		//List<Record> links = PmScheduleLinks.dao.getPmScheduleLinks(stationId);
		// List<Record> links=null;
		//从视图中获取进度
		List<Record> pm = PmSchedule.dao.getScheduleFromView(stationId);	
		renderJson(pm);
	}
	public void listData(){		
			Object[] queryParams = getQueryParams();
			String[] properties = (String[]) queryParams[0];
			String[] symbols = (String[]) queryParams[1];
			Object[] values = (Object[]) queryParams[2];			
			String orderBy = getOrderBy();
			if(StringUtil.isEmpty(orderBy)) {
				orderBy = "id asc";
			}			
			List<Record> list = DBTool.findByMultProperties("pm_schedule", properties, symbols, values, orderBy, getPager());		
			//ZcurdTool.replaceDict(PmStation.dao.getDictDataStation_name(), list, "station_id");
			renderDatagrid(
				list, 
				DBTool.countByMultProperties("pm_schedule", properties, symbols, values)
			);
		}
	public void addPage(){
		setAttr("parent", getPara("parent"));
		setAttr("station_id", getPara("station_id"));
		setAttr("station_name", getPara("station_name"));		
		render("add.html");
	}
	public void add(){
		getModel(PmSchedule.class, "model").save();
		renderSuccess();
	}
	public void updatePage(){
		setAttr("model", new PmSchedule().findById(getPara("id")));
		render("update.html");
	}
	
	public void update(){
		getModel(PmSchedule.class, "model").update();
		renderSuccess();
	}
	public void delete(){
		
		new PmSchedule().findById(getPara("id")).delete();
	}
}
