package com.busi.pm.controller;

import java.util.List;

import com.busi.pm.model.PmPerson;
import com.frame.common.tool.DBTool;
import com.frame.common.util.StringUtil;
import com.frame.controller.BaseController;
import com.jfinal.plugin.activerecord.Record;

public class PersonController extends BaseController {
	public void index(){
		setAttr("task_id", getPara("task_id"));
		setAttr("station_id", getPara("station_id"));
		render("index.html");
	}	
	public void listData(){		
			Object[] queryParams = getQueryParams();
			String[] properties = (String[]) queryParams[0];
			String[] symbols = (String[]) queryParams[1];
			Object[] values = (Object[]) queryParams[2];			
			String orderBy = getOrderBy();
			if(StringUtil.isEmpty(orderBy)) {
				orderBy = "id asc";
			}			
			List<Record> list = DBTool.findByMultProperties("pm_schedule_person", properties, symbols, values, orderBy, getPager());		
			
			renderDatagrid(
				list, 
				DBTool.countByMultProperties("pm_schedule_person", properties, symbols, values)
			);
		}
	public void addPage(){
		setAttr("task_id", getPara("queryParams[task_id]"));
		setAttr("station_id", getPara("queryParams[station_id]"));
		render("add.html");
	}
	public void add(){
		getModel(PmPerson.class, "model").save();
		renderSuccess();
	}
	public void updatePage(){
		setAttr("model", new PmPerson().findById(getPara("id")));
		render("update.html");
	}
	public void update(){
		getModel(PmPerson.class, "model").update();
		renderSuccess();
	}
	public void delete(){		
		
		Integer[] ids = getParaValuesToInt("id[]");
		for (Integer id : ids) {
			new PmPerson().set("id", id).delete();			
		}
		renderSuccess();
	}
}
