package com.busi.pm.controller;

import java.util.List;

import com.busi.pm.model.PmSchedule;
import com.frame.controller.BaseController;
import com.jfinal.plugin.activerecord.Record;

public class BoardController extends BaseController {
	
	/**
	 * 公共看板首页
	 */
	public void index(){
		setAttr("title", "环线项目管理系统");	
		/*setAttr("sysuser", getSessionUser());
		List<PmStation> ps=PmStation.dao.getStationList();	
		List<Record> data = Db.find("select * from pm_progress_v  order by id asc");	
		setAttr("stations", data);*/
		render("index.html");
	}
	/**
	 * highcharts所有站点图表
	 */
	public void getStationProgress(){		
		List<Record> data = PmSchedule.dao.getAllStationProgress();	
		renderJson(data);		
	}
	/**
	 * highcharts详细站点主要计划进度
	 */
	public void getKeySchedule(){
		List<PmSchedule> data = PmSchedule.dao.getKeySchedule(getPara("station_name"));		
		renderJson(data);
	}
}
