package com.busi.pm.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

public class Pm_MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp){
	arp.addMapping("pm_schedule", PmSchedule.class);
	arp.addMapping("pm_schedule_links", PmScheduleLinks.class);
	arp.addMapping("pm_shcedule_material", PmMaterial.class);
	arp.addMapping("pm_schedule_person", PmPerson.class);
	arp.addMapping("pm_station", PmStation.class);
	arp.addMapping("pm_worklog", PmWorkLog.class);
	arp.addMapping("pm_schedule_equipment", PmEquipment.class);
	arp.addMapping("pm_question_feedback", PmQuestion.class);
	}

}
