package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmPerson extends Model<PmPerson> {
	public static final PmPerson dao =new PmPerson();
	public List<PmPerson> getPmPerson(String taskId){
		String sqlPara="select * from pm_schedule_person where task_id=?";
		return PmPerson.dao.find(sqlPara,taskId);
		
	}

}
