package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmMaterial extends Model<PmMaterial> {
	public static final PmMaterial dao =new PmMaterial();
	
	public List<PmMaterial> getPmMaterial(String taskId){
		String sqlPara="select * from pm_shcedule_material where task_id=?";
		return PmMaterial.dao.find(sqlPara,taskId);
	}

}
