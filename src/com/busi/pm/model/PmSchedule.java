package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

@SuppressWarnings("serial")
public class PmSchedule extends Model<PmSchedule> {
	
public static final PmSchedule dao =new PmSchedule();	
	
	//获得所有站点
	public List<Record> getAllStationProgress(){
		String sql="select * from pm_progress_v";
	    return Db.find(sql);
	}
	//获得大节点进度
	public List<PmSchedule> getKeySchedule(String name){
		String sql="select * from pm_schedule where parent=0 and station_name=?";
	    return PmSchedule.dao.find(sql,name);
	}
	public List<Record> getScheduleFromView(String station_id){
		String sql="select * from pm_shcedule_v where station_id=?";
	    return Db.find(sql,station_id);
	}
}	
