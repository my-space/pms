package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmQuestion extends Model<PmQuestion> {
	public static final PmQuestion dao =new PmQuestion();

	public List<PmQuestion> getQuestions(String station_id) {
		String sqlPara="select top 10 * from pm_question_feedback where station_id=? order by id desc";
		return PmQuestion.dao.find(sqlPara,station_id);
	}

}
