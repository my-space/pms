package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

@SuppressWarnings("serial")
public class PmScheduleLinks extends Model<PmScheduleLinks> {
	
	public static final PmScheduleLinks dao = new PmScheduleLinks();

	public List<Record> getPmScheduleLinks(String id){		
		String sql="select * from pm_schedule_links where station_id=?";		
		return Db.find(sql,id);
	}
}
