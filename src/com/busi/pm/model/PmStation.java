package com.busi.pm.model;

import java.util.List;
import java.util.Map;

import com.frame.common.tool.DbMetaTool;
import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmStation extends Model<PmStation> {
	public static final PmStation dao =new PmStation();
	/**
	 * 根据车站ID获取车站名
	 * @param station_id
	 * @return
	 */
	public String getStationNameById(String station_id){
		String sqlPara="select name from pm_station where code=?";
		PmStation pm=PmStation.dao.findFirst(sqlPara,station_id);
		return pm.getStr("name");
	}
	public List<PmStation> getStationList(){
		String sqlPara="select * from pm_station";
		return PmStation.dao.find(sqlPara);
	}
	public Map<String, Object> getDictDataStation_name() {
		return DbMetaTool.getDictData("select code as 'key',name as 'text' from pm_station");
	}

}
