package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmWorkLog extends Model<PmWorkLog> {
	
	public static final PmWorkLog dao=new PmWorkLog();
	
	
	public List<PmWorkLog> getWorkLog(String station_id){
		String sqlPara="select top 10 * from pm_worklog where station_id=? order by id desc";
		return PmWorkLog.dao.find(sqlPara,station_id);
		
	}

}
