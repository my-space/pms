package com.busi.pm.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PmEquipment extends Model<PmEquipment> {
	public static final PmEquipment dao =new PmEquipment();

	public List<PmEquipment> getEquipments(String task_id) {
		String sqlPara="select * from pm_schedule_equipment where task_id=?";
		return PmEquipment.dao.find(sqlPara,task_id);
	}

}
