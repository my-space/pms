package com.frame.vo;

/**
 * 字典模型
 * @author xl
 *
 */
public class ZcurdDict {	
	
	public String key;
	public String text;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
