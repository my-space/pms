package com.frame.common.tool;

public class ErrorMsgException extends RuntimeException {

	public ErrorMsgException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
