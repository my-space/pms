package com.frame.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.jfinal.kit.PropKit;

/**
 * 访问地址处理类
 * @author xpg
 *
 */
public class UrlUtil {

	/**
	 * 获得url的基础地址（到action部分，用于权限管理）
	 */
	public static String formatBaseUrl(String url) {
		url = url.replaceAll("//+", "/").replaceAll("/$", "");
		if(url.split("/").length >= 2) {
			url = url.replaceAll("/\\w*$", "");
		}
		return url;
	}
	
	public static String formatUrl(String url) {
		url = url.replaceAll("//+", "/").replaceAll("/$", "");
		return url;
	}
	/**
	 * 去掉url参数
	 */
	public static String removeUrlParam(String url) {
		return url.replaceAll("[?#].*", "");
	}
	/**
	 * 
	 * @param url
	 * @return
	 */
	public static String getAsText(String url) {
		String urlString = "";
		try {
			URLConnection urlConnection = new URL(url).openConnection();
			urlConnection.setConnectTimeout(1000 * 30);	//30秒超时
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String current;
			while ((current = in.readLine()) != null) {
				urlString += current;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return urlString;
	}

	/**
	 * @author xpg 2016.11.20
	 * 通过对配置文件“#允许访问URL规则(比如对外的API接口)”内容，排除对外接口排除拦截
	 * @return
	 */
	public static boolean ExcludeUrl(String contextPath,String currUrl){
		//PropKit.use("a_little_config.txt");
		String[] urls=PropKit.get("noneAuth").split(",");
		boolean bool=true;
		int sum=0;
		for(int i=0;i<urls.length;i++){
			if(currUrl.startsWith(contextPath+urls[i])){
				sum++;
			}
			if(sum>0){
				bool=false;
			}
		}		
		return bool;		
		
	}
}
