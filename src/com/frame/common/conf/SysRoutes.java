package com.frame.common.conf;

import com.frame.controller.CommonController;
import com.frame.controller.LoginController;
import com.frame.controller.MainController;
import com.frame.controller.MenuController;
import com.frame.controller.RoleController;
import com.frame.controller.SysOplogController;
import com.frame.controller.TaskBaseController;
import com.frame.controller.UserController;
import com.frame.controller.ZcurdController;
import com.frame.controller.ZcurdHeadController;
import com.jfinal.config.Routes;

public class SysRoutes extends Routes {

	@Override
	public void config() {
		// TODO Auto-generated method stub
		add("/login", LoginController.class, "frame/login");
		add("/zcurd", ZcurdController.class, "frame/zcurd");
		add("/zcurdHead", ZcurdHeadController.class, "frame");
		add("/menu", MenuController.class, "frame/menu");
		add("/main", MainController.class, "frame");
		add("/role", RoleController.class, "frame/role");
		add("/common", CommonController.class, "frame");
		add("/oplog", SysOplogController.class, "frame/sysOplog");
		add("/user", UserController.class, "frame/user");		
		add("/task", TaskBaseController.class, "frame/taskBase");
	}

}
