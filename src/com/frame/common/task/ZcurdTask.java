package com.frame.common.task;

import com.frame.common.util.DateUtils;
import com.frame.common.util.StringUtil;
import com.frame.common.util.UrlUtil;
import com.frame.model.TaskBase;
import com.frame.model.TaskLog;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.cron4j.ITask;

public class ZcurdTask implements ITask {
	private int id;	//taskBaseId
	private int taskTargetType;	//任务类型
	private String taskTargetValue;	//任务值

	public ZcurdTask(int taskTargetType, String taskTargetValue) {
		this.taskTargetType = taskTargetType;
		this.taskTargetValue = taskTargetValue;
	}
	
	public ZcurdTask(int id, int taskTargetType, String taskTargetValue) {
		this.id = id;
		this.taskTargetType = taskTargetType;
		this.taskTargetValue = taskTargetValue;
	}
	
	@Override
	public void run() {
		TaskLog log = new TaskLog();		
		String startDate = DateUtils.getNowDateTimeLong();	
		//System.out.println(startDate);
		log.set("task_id", id).set("start_time", startDate).save();	
		
		String result = "成功";
		try {
			for (final String value : taskTargetValue.trim().split(";")) {
				if(StringUtil.isEmpty(value)) {
					continue;
				}
				
				if(taskTargetType == TaskConstant.TASK_TARGET_TYPE1) {
					 String content = UrlUtil.getAsText(value);
					System.out.println("????"+content);// TODO 执行结果判断
					
				}else if(taskTargetType == TaskConstant.TASK_TARGET_TYPE2) {
					
					try {
						Db.find(value);
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
					
					
					//权限太大慎用，可执行任意SQL
					/*Db.execute(new ICallback() {
						@Override
						public Object call(Connection conn) throws SQLException {
							conn.createStatement().execute(value);
							return true;
						}
					});*/
					
				}else if(taskTargetType == TaskConstant.TASK_TARGET_TYPE3) {
					ITask task = (ITask) Class.forName(value).newInstance();
					task.run();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "失败";
			//log.setRemark(e.getMessage());
		}
		
		String endDate = DateUtils.getNowDateTimeLong();		
		Long costTime = DateUtils.getIntervalMsec(startDate,endDate);
		//System.out.println("！！！！"+costTime);
		log.set("result", result);
		log.set("end_time",endDate);
		log.set("cost_time",costTime);
		log.update();
		
		//更新任务运行信息
		if(id > 0) {
			TaskBase.me.findById(id)
				.set("last_run_result",result)
				.set("last_run_time",startDate)
				.set("last_run_time_cost",costTime)
				/*.set("status",3)*/
				.update();
		}
		System.out.println(DateUtils.getNowDateTime() + "定时任务执行完成");
	}

	@Override
	public void stop() {
		System.out.println("top");
	}

}
