package com.frame.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.frame.common.tool.DBTool;
import com.frame.common.util.MD5Util;
import com.frame.common.util.StringUtil;
import com.frame.ext.render.csv.CsvRender;
import com.frame.model.SysUser;
import com.jfinal.plugin.activerecord.Record;

public class UserController extends BaseController {
		
	
	public void listPage() {
		setAttr("dictDataroles", SysUser.me.getDictDataroles());
		render("list.html");
	}
	
	public void listData() {
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		
		String orderBy = getOrderBy();
		if(StringUtil.isEmpty(orderBy)) {
			orderBy = "id desc";
		}		
		String fieldName = "roles";		
		List<Record> list = DBTool.findByMultProperties("sys_user", properties, symbols, values, orderBy, getPager());
		Map<String, Object> dictDataroles = SysUser.me.getDictDataroles();
		/*for(Record record : list) {			
			set=record.get(fieldName).toString().split(",");	
			System.out.println(set.length);
			if(set.length==1){
				if(dictDataroles.get(set[0]) != null) {
				record.set(fieldName, dictDataroles.get(set[0]));
				}
			}
			else {
				for(int i=0;i<set.length;i++ ){
					if(dictDataroles.get(set[i]) != null) {
						dictfield+=dictDataroles.get(set[0]).toString();
						dictfield+=",";
						}
				}
				record.set(fieldName, dictfield.substring(0, dictfield.length()-1));
				
			}
		}*/
		setDictOfValues(list,dictDataroles,fieldName);
		renderDatagrid(
			list, 
			DBTool.countByMultProperties("sys_user", properties, symbols, values)
		);
	}
	
	//增加页面
	public void addPage() {
		setAttr("dictDataroles", SysUser.me.getDictDataroles());
		render("add.html");
	}
	
	//增加
	public void add() {
		SysUser sysuser=getModel(SysUser.class, "model");
		try {
			if(StringUtil.isEmpty(sysuser.get("password")))
			{
				sysuser.set("password", MD5Util.md5Digest(MD5Util.INITPASSWORD));
			}
			else{
				sysuser.set("password", MD5Util.md5Digest(sysuser.get("password")));
			}
			
		} catch (Exception e) {
			System.out.println("密码加密失败");
		}
		sysuser.save();
		addOpLog("[用户管理] 增加");
		renderSuccess();
	}
	
	//修改页面
	public void updatePage() {
		setAttr("dictDataroles", SysUser.me.getDictDataroles());
		setAttr("model", SysUser.me.findById(getPara("id")));
		render("update.html");
	}
	
	//修改
	public void update() {
		SysUser model = SysUser.me.findById(getPara("id"));
		model.set("user_name", getPara("model.user_name"));
		try {
			if(StringUtil.isEmpty(model.get("password")))
			{
				model.set("password", MD5Util.md5Digest(MD5Util.INITPASSWORD));
			}
			else {
				model.set("password", MD5Util.md5Digest(getPara("model.password")));
			}			
		} catch (Exception e) {
			System.out.println("密码加密失败");
		}
		model.set("roles", getPara("model.roles"));
		model.update();
		addOpLog("[用户管理] 修改");
		renderSuccess();
	}
	
	//删除
	public void delete() {
		Integer[] ids = getParaValuesToInt("id[]");
		for (Integer id : ids) {
			new SysUser().set("id", id).delete();
			
		}
		
		addOpLog("[用户管理] 删除");
		renderSuccess();
	}
	//修改密码	
	public void modifyPassword() {	
		int ret=0;
		if(StringUtil.isEmptyAny(getPara("password"),getPara("repassword"))){
			renderFailed(" 密码不能为空");
		}
		else if(!StringUtil.equals(getPara("password"),getPara("repassword"))){
			renderFailed(" 两次密码不一致");
		}
		else if(StringUtil.equals(getPara("password"),getPara("repassword"))){
			
			try {
				ret=SysUser.me.setPassword(getSessionUser().get("user_name"),MD5Util.md5Digest(getPara("password")));
			} catch (Exception e) {
				System.out.println("修改密码错误，加密失败！");
			}
			if(ret>0){
			addOpLog("用户"+getSessionUser().get("user_name")+"修改密码");
			renderSuccess("修改成功");
			}
			else{
				renderFailed("修改失败");
			}
		}
		
	}
    //重置密码	
	public void resetPassword() {	
	    SysUser model = SysUser.me.findById(getPara("id"));
	    boolean ret = false;
	    try {
			model.set("password", MD5Util.md5Digest(MD5Util.INITPASSWORD));
			ret=model.update();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("用户"+model.get("user_name")+"加密失败");;
		}
	    if(ret==true){
			addOpLog("用户"+model.get("user_name")+"重置密码成功");					
			renderSuccess("密码已重置为123456");	
	    }
	    else
	    { 
	    	renderFailed("重置密码失败");	
	    }
	}
	//详情页面
	public void detailPage() {
		SysUser model = SysUser.me.findById(getParaToInt("id"));
		Map<String, Object> dictDataroles = SysUser.me.getDictDataroles();
		/*if(dictDataroles.get(model.get("roles").toString()) != null) {
			model.set("roles", dictDataroles.get(model.get("roles").toString()));
		}*/
		setDictOfValues(model,dictDataroles,"roles");
		setAttr("model", model);
		render("detail.html");
	}
	
	//导出csv
	public void exportCsv() {
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		
		String orderBy = getOrderBy();
		if(StringUtil.isEmpty(orderBy)) {
			orderBy = "id desc";
		}
		
		List<Record> list = DBTool.findByMultProperties("sys_user", properties, symbols, values);
		Map<String, Object> dictDataroles = SysUser.me.getDictDataroles();
		for(Record record : list) {
			String fieldName = "roles";
			if(dictDataroles.get(record.get(fieldName).toString()) != null) {
				record.set(fieldName, dictDataroles.get(record.get(fieldName).toString()));
			}
		}
		
		List<String> headers = new ArrayList<String>();
		List<String> clomuns = new ArrayList<String>();
		headers.add("用户名");
		clomuns.add("user_name");
		headers.add("密码");
		clomuns.add("password");
		headers.add("角色");
		clomuns.add("roles");
		
		CsvRender csvRender = new CsvRender(headers, list);
		csvRender.clomuns(clomuns);
		csvRender.fileName("用户管理");
		
		addOpLog("[用户管理] 导出cvs");
		render(csvRender);
	}		
}
