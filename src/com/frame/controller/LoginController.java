package com.frame.controller;

import java.util.List;

import com.frame.common.util.MD5Util;
import com.frame.model.Menu;
import com.frame.model.SysUser;
import com.frame.service.LoginService;
import com.jfinal.aop.Duang;

/**
 * 登陆
 * @author xpg
 */
public class LoginController extends BaseController {
	
	public void index() {
		render("login.html");
	}
	
	public void getMenu() {				
		//admin用户拥有所有页面
		if("admin".equals(getSessionUser().get("user_name"))) {
			renderJson(Menu.me.findAll());
		}else {
//			renderJson(getSessionAttr("menuList").toString());
//			System.out.println("22222222222"+getSessionAttr("menuList").toString());
//			System.out.println("44444444444"+new LoginService().getUserMenu(getSessionUser()));
			renderJson(new LoginService().getUserMenu(getSessionUser()));
		}
	}
	
	public void login() {
		LoginService loginService = Duang.duang(LoginService.class);
		List<SysUser> list = null;
		try {
			list = SysUser.me.findByMultiProperties(new String[]{"user_name", "password"}, 
					new Object[]{getPara("user_name"), MD5Util.md5Digest(getPara("password"))});
		} catch (Exception e) {
			renderFailed("登陆失败，用户名密码错误！");
		}
		if(list.size() > 0) {
			setSessionAttr("sysUser", list.get(0));
			//用户菜单
			setSessionAttr("menuList", loginService.getUserMenu(getSessionUser()));
			//页面权限
			List<String> noAuthUrl = loginService.getNoAuthUrl();
			setSessionAttr("noAuthUrl", noAuthUrl);
			//按钮权限
			setSessionAttr("noAuthBtnUrl", loginService.getNoAuthBtnUrl(getSessionUser()));
			//数据权限
			setSessionAttr("noAuthDatarule", loginService.getNoAuthDatarule(getSessionUser()));
			if("admin".equals(getSessionUser().get("user_name"))) {
				setSessionAttr("noAuthUrl", null);
				setSessionAttr("noAuthBtnUrl", null);
				setSessionAttr("pageBtnMap", null);
				setSessionAttr("noAuthDatarule", null);
			}
			addOpLog("登陆系统");
			renderSuccess();
		}else {
			renderFailed("用户名或密码错误！");
		}
	}
	
	public void logout() {
		addOpLog("退出系统");
		removeSessionAttr("sysUser");
		redirect("/login");
	}	
}
