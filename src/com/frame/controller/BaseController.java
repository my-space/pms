package com.frame.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.frame.common.tool.Pager;
import com.frame.model.MenuDatarule;
import com.frame.model.SysOplog;
import com.frame.model.SysUser;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/**
 * 自定义控制器基类
 * @author xpg
 *
 */
public class BaseController extends Controller {
	
	protected void renderDatagrid(Page<?> pageData) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("rows", pageData.getList());
		datagrid.put("total", pageData.getTotalRow());
		renderJson(datagrid);
	}
	
	protected void renderDatagrid(List<?> list, int total) {
		renderDatagrid(list, total, null);
	}
	
	protected void renderDatagrid(List<?> list, int total, List<Map<String, Object>> footer) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("rows", list);
		datagrid.put("total", total);
		if(footer != null && footer.size() > 0) {
			datagrid.put("footer", footer);
		}
		renderJson(datagrid);
	}
	
	protected void renderDatagrid(List<Record> list) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("rows", list);
		renderJson(datagrid);
	}
	/**
	 * 渲染Dhtml Gantt图
	 * @param data
	 * @param links
	 */
	protected void renderDhtmlGantt(List<Record> data,List<Record> links) {
		Map<String, Object> result = new HashMap<String, Object>();	
		result.put("data", data);
		result.put("links", links);
		renderJson(result);
	}	
	protected void renderSuccess(String msg) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "success");
		result.put("msg", msg);
		renderJson(result);
	}
	
	protected void renderSuccess() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "success");
		renderJson(result);
	}
	
	protected void renderFailed(String msg) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "fail");
		result.put("msg", msg);
		renderJson(result);
	}
	
	protected void renderFailed() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "fail");
		renderJson(result);
	}
	
	protected SysUser getSessionUser() {
		return getSessionAttr("sysUser");
	}
	
	protected Pager getPager() {
		Pager pager = new Pager();
		pager.setPage(getParaToInt("page", 0));
		pager.setRows(getParaToInt("rows", 0));
		return pager;
	}
	
	protected Object[] getQueryParams() {
		List<String> properties = new ArrayList<String>();
		List<String> symbols = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();
		
		Map<String, String[]> paraMap = getParaMap();
		for (String paraName : paraMap.keySet()) {
			String prefix = "queryParams[";
			if(paraName.startsWith(prefix)) {
				String field = paraName.substring(prefix.length(), paraName.length() - 1);
				String symbol = "=";
				String value = paraMap.get(paraName)[0];
				
				//处理范围参数
				if(field.startsWith("_start_")) {
					field = field.replaceAll("^_start_", "");
					symbol = ">=";
				}else if(field.startsWith("_end_")) {
					field = field.replaceAll("^_end_", "");
					symbol = "<=";
				}
				
				//模糊搜索处理
				if(value.startsWith("*") && value.endsWith("*")) {
					try{
					value = "%" + value.substring(1, value.length() - 1) + "%";
					symbol = "like";
					}
					catch (Exception e) {
						new Throwable(e.getMessage());
					}
					
				}else if(value.startsWith("*")) {
					value = "%" + value.substring(1);
					symbol = "like";
				}else if(value.endsWith("*")) {
					value = value.substring(0, value.length() - 1) + "%";
					symbol = "like";
				}
				
				properties.add(field);
				symbols.add(symbol);
				values.add(value);
			}
		}
		//数据权限处理
		if(getAttr("menuDataruleList") != null) {
			List<MenuDatarule> menuDataruleList = getAttr("menuDataruleList");
			for (MenuDatarule menuDatarule : menuDataruleList) {
				properties.add(menuDatarule.getFieldName());
				symbols.add(menuDatarule.getSymbol());
				values.add(menuDatarule.getValue());
			}
		}
		return new Object[]{properties.toArray(new String[]{}), symbols.toArray(new String[]{}), values.toArray(new Object[]{})};
	}
	
	protected String getOrderBy() {
		String sqlOrderBy = ""; 
		Map<String, String[]> paraMap = getParaMap();
		if(paraMap.get("sort") != null && paraMap.get("sort").length > 0) {
			String[] sort = paraMap.get("sort")[0].split(",");
			String[] order = paraMap.get("order")[0].split(",");
			sqlOrderBy = sort[0] + " " + order[0];
			for (int i = 1; i < sort.length; i++) {
				sqlOrderBy += ", " + sort[i] + " " + order[i];
			}
		}
		return sqlOrderBy;
	}
	/**
	 *单个数据列中多个字典项的替换
	 *@author xpg
	 */
	public void setDictOfValues(List<Record> list,Map<String, Object> dictMap,String fieldName){
		String[] set;
		String dictfield="";
		for(Record record : list) {			
			set=record.get(fieldName).toString().split(",");			
			if(set.length==1){
				if(dictMap.get(set[0]) != null) {
				record.set(fieldName, dictMap.get(set[0]));
				}
			}
			else if(set.length>1){
				for(int i=0;i<set.length;i++ ){
					if(dictMap.get(set[i]) != null) {						
						dictfield+=dictMap.get(set[i]).toString();
						dictfield+=",";
						}
				}
				record.set(fieldName, dictfield.substring(0, dictfield.length()-1));
				
			}
		}
	}
	/**
	 *单个数据列中多个字典项的替换
	 *@author xpg
	 */
	@SuppressWarnings("rawtypes")
	public void setDictOfValues(Model model,Map<String, Object> dictMap,String fieldName){
		String[] set;
		String dictfield="";			
		set=model.get(fieldName).toString().split(",");			
		if(set.length==1){
			if(dictMap.get(set[0]) != null) {
			model.set(fieldName, dictMap.get(set[0]));
			}
		}
		else if(set.length>1){
			for(int i=0;i<set.length;i++ ){
				if(dictMap.get(set[i]) != null) {						
					dictfield+=dictMap.get(set[i]).toString();
					dictfield+=",";
					}
			}
			model.set(fieldName, dictfield.substring(0, dictfield.length()-1));
			
		}
	
	}
	/**
	 * 增加操作日志
	 * @param opContent 操作内容
	 */
	protected void addOpLog(String opContent) {
		SysOplog.me
			.remove("id")
			.set("user_id", getSessionUser().get("id"))
			.set("op_content", opContent)
			.set("ip", getRemoteAddress())			
			.save();
	}
	
	/**
	 * 获得ip地址
	 */
	protected String getRemoteAddress() {
		String ip = getRequest().getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = getRequest().getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = getRequest().getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = getRequest().getRemoteAddr();
		}
		return ip;
	}
}
