package com.frame.controller;

import com.frame.common.tool.DBTool;
import com.frame.common.util.StringUtil;
import com.frame.model.CommonFile;
import com.jfinal.upload.UploadFile;

/**
 * 公共方法地址,首页控制器
 * @author xpg
 */
public class CommonController extends BaseController {
	
	//获得字典数据
	public void getDictData() {
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		
		String orderBy = getOrderBy();
		if(StringUtil.isEmpty(orderBy)) {
			orderBy = "id desc";
		}
		
		renderJson(DBTool.findByMultProperties("sys_dict", properties, symbols, values));
	}	
	
	//图标页面
	public void iconsPage() {
		render("common/icons.html");
	}
	//文件上传
	public void uploadFile() {
		UploadFile file = getFile("upload", "images", 1024 * 1024 * 5);	//上传文件不能大于5M
		if(file != null) {
			//保存日志
			new CommonFile().set("type", 1)	//1表示图片
				.set("path", file.getFileName())
				.set("sys_user_id", getSessionUser().get("id"))
				.save();
			renderJson("/upload/images/" + file.getFileName());
		}else {
			renderFailed();
		}
	}

	//首页
	public void indexPage() {
		//设置温度
		setAttr("template", 18);
		//设置湿度
		setAttr("humidity", "78%");
		
		render("index/index.html");
	}

}
