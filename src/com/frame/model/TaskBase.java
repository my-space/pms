package com.frame.model;

import java.util.Map;

import com.frame.common.tool.DbMetaTool;
import com.jfinal.plugin.activerecord.Model;

public class TaskBase extends Model<TaskBase> {
	private static final long serialVersionUID = 1L;
	public static final TaskBase me = new TaskBase();
	
	public Map<String, Object> getDictDatatarget_type() {
		return DbMetaTool.getDictData("select dict_key, dict_value from sys_dict where dict_type='task_type'");
	}	
		
	public Map<String, Object> getDictDatastatus() {
		return DbMetaTool.getDictData("select dict_key, dict_value from sys_dict where dict_type='task_statu'");
	}
}
