package com.frame.model;

import java.util.List;
import java.util.Map;

import com.frame.common.tool.DbMetaTool;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

public class SysUser extends Model<SysUser> {
	private static final long serialVersionUID = 1L;
	public static final SysUser me = new SysUser();
	
	public List<SysUser> findByMultiProperties(String[] properties, Object[] values) {
		StringBuffer sql = new StringBuffer("select * from sys_user where 1=1");
		if(properties != null) {
			for (String property : properties) {
				sql.append(" and " + property + "=?");
			}
		}
		if(values == null) {
			values = new Object[]{};
		}
		return find(sql.toString(), values);
	}
	
	/*private Table getTable() {
		return TableMapping.me().getTable(getUsefulClass());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Class<? extends Model> getUsefulClass() {
		Class c = getClass();
		return c.getName().indexOf("EnhancerByCGLIB") == -1 ? c : c.getSuperclass();	// com.demo.blog.Blog$$EnhancerByCGLIB$$69a17158
	}*/
	
	//用户角色字典
	public Map<String, Object> getDictDataroles() {
		return DbMetaTool.getDictData("select cast(id as varchar) as id, role_name from sys_role");
	}
	
	/**
	 * 设置密码
	 * @return
	 */	
	public int setPassword(String username,String newpassword) {		
		return Db.update("update sys_user set password='"+newpassword+"' where user_name='"+username+"'");		 
		
	}
}
