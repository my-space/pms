package com.frame.model;

import java.util.Map;

import com.frame.common.tool.DbMetaTool;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author xpg
 *
 */
public class SysOplog extends Model<SysOplog> {
	private static final long serialVersionUID = 1L;
	public static final SysOplog me = new SysOplog();
		
	public Map<String, Object> getDictDatauser_id() {
		return DbMetaTool.getDictData("select cast(id as varchar)as id, user_name from sys_user ");
	}	
	
}
