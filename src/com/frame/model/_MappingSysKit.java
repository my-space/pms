package com.frame.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.frame.model.CommonFile;
import com.frame.model.TaskBase;
import com.frame.model.TaskLog;

public class _MappingSysKit {
	
	/**
	 * 系统表和Model统一映射
	 * @param arp
	 */
	public static void mapping(ActiveRecordPlugin arp) {		
		arp.addMapping("zcurd_head", ZcurdHead.class);
		arp.addMapping("zcurd_field", ZcurdField.class);
		arp.addMapping("zcurd_head_btn", ZcurdHeadBtn.class);
		arp.addMapping("zcurd_head_js", ZcurdHeadJs.class);
		arp.addMapping("sys_menu", Menu.class);
		arp.addMapping("sys_menu_btn", MenuBtn.class);
		arp.addMapping("sys_menu_datarule", MenuDatarule.class);
		arp.addMapping("sys_user", SysUser.class);
		arp.addMapping("sys_menu_btn", SysMenuBtn.class);
		arp.addMapping("sys_oplog", SysOplog.class);			
		arp.addMapping("sys_file", CommonFile.class);
		arp.addMapping("sys_task", TaskBase.class);
		arp.addMapping("sys_tklog", "id", TaskLog.class);
	}

}
