package com.frame.service;

import com.frame.common.util.StringUtil;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 角色权限管理（权限实现请查看LoginService）
 * @author xpg 2016.2.17
 */
public class RoleService {
	
	/**
	 * 保存权限
	 */
	@Before(Tx.class)
	public void saveAuth(String menuIds, String btnIds, String dataruleIds, int roleId) {
		//保存菜单权限
		Db.update("delete from sys_role_menu where role_id=?", roleId);
		if(StringUtil.isNotEmpty(menuIds)) {
			for (String menuId : menuIds.split(",")) {
				Db.update("insert into sys_role_menu (role_id, menu_id) values (?, ?)", new Object[]{roleId, menuId});
			}
		}
		//保存按钮权限
		Db.update("delete from sys_role_btn where role_id=?", roleId);
		if(StringUtil.isNotEmpty(btnIds)) {
			for (String btnId : btnIds.split(",")) {
				Db.update("insert into sys_role_btn (role_id, btn_id) values (?, ?)", new Object[]{roleId, btnId});
			}
		}
		//保存数据权限
		Db.update("delete from sys_role_datarule where role_id=?", roleId);
		if(StringUtil.isNotEmpty(dataruleIds)) {
			for (String dataruleId : dataruleIds.split(",")) {
				Db.update("insert into sys_role_datarule (role_id, datarule_id) values (?, ?)", new Object[]{roleId, dataruleId});
			}
		}		
	}

}
