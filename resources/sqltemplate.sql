---------获得工作经历信息----------
#sql("getworkexperience")
select in_date,case when convert(varchar(40),out_date,23) is null then '今' else convert(varchar(40),out_date,23) end as out_date,work_place,work_position from hr_work_experience where p_no=?
#end

#sql("getPersonCount")
select nnd as age,gender, count(*) as c_num from(
select
case
when age>=21 and age<=25 then '20-25'
when age>=26 and age<=30 then '25-30'
when age>=31 and age<=35 then '30-35'
when age>=36 and age<=40 then '35-40'
when age>=41 and age<=45 then '40-45'
when age>=46 and age<=50 then '45-50'
when age>=51 and age<=55 then '50-55'
when age>=56 and age<=60 then '55-60'
else '大于60'
end
as nnd,gender from person
) person group by gender,nnd order by nnd
#end
